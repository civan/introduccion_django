from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^autores/$', AutoresListView.as_view(), name='autores-lista'),
    url(r'^autores/(?P<pk>[-\d]+)/$', AutoresDetailView.as_view(), name='autores-detalle'),
    url(r'^libros/$', LibrosListView.as_view(), name='libros-lista'),
    url(r'^libros/(?P<pk>[-\d]+)/$', LibrosDetailView.as_view(), name='libros-detalle'),
    url(r'^estanterias/$', EstanteriasListView.as_view(), name='estanterias-lista'),
    url(r'^estanterias/(?P<pk>[-\d]+)/$', EstanteriasDetailView.as_view(), name='estanterias-detalle'),
]
