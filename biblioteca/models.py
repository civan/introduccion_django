from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Estanteria(models.Model):
    BALDAS = (
    ('A', 'Balda A'),
    ('B', 'Balda B'),
    ('C', 'Balda C'),
    ('D', 'Balda D'),
    )

    numero = models.IntegerField(validators=[
        MinValueValidator(1),
        MaxValueValidator(10)
    ]) #Validación directa en el modelo, solo tengo estanterías de la 1 a la 10
    balda = models.CharField(
    max_length=1,
    choices=BALDAS,
    default='A',
    )

    def __str__(self):
        return '%s - %s' % (self.numero, self.balda)

    class Meta:
        verbose_name_plural = 'Estanterías'
        ordering = ['numero', 'balda', ]


class Autor(models.Model):
    nombre = models.CharField(max_length=200)
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return '%s' % self.nombre

    class Meta:
        verbose_name_plural = 'Autores'
        ordering = ['nombre', ]


class Libro(models.Model):
    nombre = models.CharField(max_length=200)
    isbn = models.CharField(max_length=13) #restricción máxima a 13 caracteres, pero luego hay que validar el formulario.
    autores = models.ManyToManyField(Autor) #relacion al modelo Autor que ya teníamos creado
    estanteria = models.ForeignKey(Estanteria, blank=True, null=True)

    def __str__(self):
        return '%s' % self.nombre

    class Meta:
        verbose_name_plural = 'Libros'
        ordering = ['nombre', ]
