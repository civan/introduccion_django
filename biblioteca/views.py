from django.shortcuts import render

from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView


from .models import Estanteria, Libro, Autor

# Create your views here.


class IndexView(TemplateView):
    template_name = "index.html"


class AutoresListView(ListView):
    model = Autor
    template_name = "autores_lista.html"
    paginate_by = 3


class AutoresDetailView(DetailView):
    model = Autor
    template_name = "autores_detalle.html"


class LibrosListView(ListView):
    model = Libro
    template_name = "libros_lista.html"
    paginate_by = 3


class LibrosDetailView(DetailView):
    model = Libro
    template_name = "libros_detalle.html"


class EstanteriasListView(ListView):
    model = Estanteria
    template_name = "estanterias_lista.html"
    paginate_by = 3


class EstanteriasDetailView(DetailView):
    model = Estanteria
    template_name = "estanterias_detalle.html"
