from django.contrib import admin

# Register your models here.

from .models import Autor, Libro, Estanteria
from .forms import LibroForm

class AutorAdmin(admin.ModelAdmin):
    model = Estanteria
    list_display = ['nombre', 'fecha_nacimiento', ]
    list_filter = ['nombre', 'fecha_nacimiento', ]
    search_fields = ['nombre', 'fecha_nacimiento', ]


class LibroAdmin(admin.ModelAdmin):
    form = LibroForm
    model = Estanteria
    list_display = ['nombre', 'isbn', 'estanteria', ]
    list_filter = ['nombre', 'isbn', 'autores', 'estanteria', ]
    search_fields = ['nombre', 'isbn', 'autores', 'estanteria', ]
    filter_horizontal = ['autores', ]


class EstanteriaAdmin(admin.ModelAdmin):
    model = Estanteria
    list_display = ['numero', 'balda', ]
    list_filter = ['numero', 'balda', ]
    search_fields = ['numero', 'balda', ]



admin.site.register(Autor, AutorAdmin)
admin.site.register(Libro, LibroAdmin)
admin.site.register(Estanteria, EstanteriaAdmin)
