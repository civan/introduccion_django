from django import forms
from biblioteca.models import Libro


class LibroForm(forms.ModelForm):

    def clean_isbn(self):
        isbn = self.cleaned_data['isbn']

        if isbn.isdigit() and (len(isbn) == 10 or len(isbn) == 13):
            return isbn
        raise forms.ValidationError("El campo ISBN no es válido")

    class Meta:
        model = Libro
        fields = ['nombre', 'isbn', 'autores', 'estanteria', ]
